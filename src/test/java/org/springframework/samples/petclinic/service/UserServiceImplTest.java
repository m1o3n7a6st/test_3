package org.springframework.samples.petclinic.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.Role;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.samples.petclinic.util.ApplicationSwaggerConfig;

import static org.mockito.Mockito.*;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = ApplicationSwaggerConfig.class)
public class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl userServiceimpl;
    @Mock
    User user;
//    @Mock
    @Spy
    UserRepository userRepository;

    Set<Role> roles=new HashSet<>();
    Role role = new Role();

    public void setMocks(int num){
      if (num ==1)
            role.setName("dad");
        else
            role.setName("ROLE_dad");
        if (num ==2)
            role.setUser(user);
        roles.add(role);
        if (num ==3)
            roles.clear();
//            when(roles.isEmpty()).thenReturn(true);
//            when(user.getRoles().isEmpty()).thenReturn(true);
        when(user.getRoles()).thenReturn(roles);
        Mockito.doNothing().when(userRepository).save(user);
    }
//    public void setMock(){
//
//    }
//

    @Before

    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);


    }


    @Test
    public void saveUser() throws Exception{
        setMocks(1);
        try {
            userServiceimpl.saveUser(user);
        }
        catch (DataAccessException e){
            fail();
        }

    }

    @Test
    public void cheak_behavioural_saveUser() throws Exception {
        setMocks(1);
        userServiceimpl.saveUser(user);
        verify( userRepository).save(user);
        verify(user,atLeast(3)).getRoles();

    }


    @Test
    public void seveUserNullRole(){
        try{
            userServiceimpl.saveUser(user);
        }
        catch (Exception e){
            assertEquals(e.getMessage(),"User must have at least a role set!");
        }

    }
    @Test
    public void seveUserNullRole1(){
        setMocks(3);
        try{
            userServiceimpl.saveUser(user);
        }
        catch (Exception e){
            assertEquals(e.getMessage(),"User must have at least a role set!");
        }

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testSaveUser() {
        setMocks(2);
        try {
            userServiceimpl.saveUser(user);
        }
        catch (DataAccessException e){
            fail();
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }

    }
}
