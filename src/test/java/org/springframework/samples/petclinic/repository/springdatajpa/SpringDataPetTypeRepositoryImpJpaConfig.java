package org.springframework.samples.petclinic.repository.springdatajpa;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages = "org.springframework.samples.petclinic")
@PropertySource("persistence-student.properties")
@EnableTransactionManagement
public class SpringDataPetTypeRepositoryImpJpaConfig {

}
