package org.springframework.samples.petclinic.repository.springdatajpa;

import org.hibernate.boot.model.relational.Database;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.samples.petclinic.model.PetType;
import org.springframework.samples.petclinic.service.UserServiceImpl;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.Transactional;

import static org.junit.Assert.*;
//@RunWith(SpringRunner.class)
//@DataJpaTest
//@SpringBootTest(properties = "spring.main.web-application-type=reactive")
//@SpringBootTest
//@RunWith(SpringJUnit4ClassRunner.class)
//@DataJpaTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
    classes = { StudentJpaConfig.class },
    loader = AnnotationConfigContextLoader.class)
@Transactional
public class SpringDataPetTypeRepositoryImplTest {
//
//    @InjectMocks

     private SpringDataPetTypeRepositoryImpl springDataPetTypeRepositoryImpl;


//    @Mock
//    public EntityManager em;
//    @Autowired
//    private EntityManager entityManager;
//    private String properties;
//    @Value("${property.reference}")
    private DataSource dataSource;

    @Before
    public void setUp() throws Exception {
        ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
        populator.addScripts(
            new ClassPathResource("schema-h2.sql"),
            new ClassPathResource("/data-h2.sql"));
        populator.setSeparator("@@");
        populator.execute(this.dataSource);
        springDataPetTypeRepositoryImpl = new SpringDataPetTypeRepositoryImpl();
//        PetType petType = new PetType();
//        petType.setId(1);
//        petType.setName("dog");
//        this.entityManager.persist(petType);
//        entityManager.createQuery()
//        MockitoAnnotations.initMocks(this);
//        Mockito.reset(em);
//        em.


    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void delete() {
//        System.out.println("hi");
        PetType petType = new PetType();
        petType.setId(1);
        petType.setName("dog");


    this.springDataPetTypeRepositoryImpl.delete(petType);
    }
}
